#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow){
    ui->setupUi(this);
    file = new QFile ("fast.txt");
    model = new QStringListModel(this);
    file->open(QIODevice::ReadOnly);
    QStringList textline;
    QTextStream stream(file);
    QString line = stream.readLine();
    while (!line.isNull()){
        textline.append(line);
        line = stream.readLine();
    }
    file->close();
    model->setStringList(textline);
    ui->listView->setModel(model);
    connect(ui->MainWindow::listView,SIGNAL(on_listView_doubleClicked(QModelIndex)),this,SLOT(on_listView_doubleClicked(QModelIndex)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::prov(QString word){
    file->open(QIODevice::ReadOnly);
    QTextStream stream(file);
    QString temp = stream.readLine();
    while(!temp.isNull()){
        if(temp==word){
            file->close();
            return false;
        }
        temp=stream.readLine();
    }
    file->close();
    return true;
}

void MainWindow::on_pushButton_clicked()
{
    QFileDialog dialog(this);
    QString f = QFileDialog::getOpenFileName(this,tr("Open Image"), "", tr("Image Files (*.png , *.bmp , *.jpg)"));
    if (f!="" && prov(f)){
        file->open(QIODevice::Append);
        file->write(std::string(f.toLocal8Bit().data()).c_str());
        file->write("\n");
        file->seek(QIODevice::ReadOnly);
        QStringList textline;
        QTextStream stream(file);
        QString line = stream.readLine();
        while (!line.isNull()){
            textline.append(line);
            line = stream.readLine();
        }
        int row = model->rowCount();
        model->insertRow(row);
        model->setData(model->index(row),f,Qt::EditRole);
        QModelIndex index = model->index(row);
        ui->listView->setCurrentIndex(index);
        ui->listView->edit(index);
        ui->listView->show();
        file->close();
    }
}


void MainWindow::on_listView_indexesMoved(const QModelIndexList &indexes)
{
}

void MainWindow::on_listView_doubleClicked(const QModelIndex &index)
{
    QString value = model->data(index,Qt::EditRole).toString();
    pictureWindow pic(NULL,value);
    pic.setModal(true);
    pic.exec();
}

void MainWindow::on_pushButton_2_clicked()
{
    QString word = ui->listView->currentIndex().data().toString();
    file->open(QIODevice::ReadOnly);
    QFile tfile("temp.txt");
    tfile.open(QIODevice::WriteOnly);
    QTextStream ts(file);
    QString temp = ts.readLine();
    QStringList templist;
    while(!temp.isNull()){
        if (temp != word){
            tfile.write(std::string(temp.toLocal8Bit().data()).c_str());
            tfile.write("\n");
        }
        temp=ts.readLine();
    }
    tfile.close();
    tfile.open(QIODevice::ReadOnly);
    file->close();
    file->open(QIODevice::WriteOnly | QIODevice::Truncate);
    file->write(tfile.readAll());
    tfile.close();
    tfile.remove();
    file->close();
    model->removeRows(ui->listView->currentIndex().row(),1);
}
