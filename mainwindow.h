#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QFile>
#include <QString>
#include <QStringListModel>
#include <picturewindow.h>
#include <QTextStream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    bool prov(QString);

private slots:
    void on_pushButton_clicked();
    void on_listView_indexesMoved(const QModelIndexList &indexes);
    void on_listView_doubleClicked(const QModelIndex &index);

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    QFile* file;
    QStringListModel* model;
};

#endif // MAINWINDOW_H
