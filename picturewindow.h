#ifndef PICTUREWINDOW_H
#define PICTUREWINDOW_H
#include <QDialog>
#include <QPixmap>
#include <QString>
#include <QMainWindow>
#include <QLabel>
#include <QGraphicsScene>
#include <QImage>

namespace Ui {
class pictureWindow;
}

class pictureWindow : public QDialog
{
    Q_OBJECT

public:
    explicit pictureWindow(QWidget *parent = nullptr,QString="");
    ~pictureWindow();

private slots:
    void on_label_linkActivated(const QString &link);

private:
    Ui::pictureWindow *ui;
    QLabel *image;
    QImage *im;

};

#endif // PICTUREWINDOW_H
